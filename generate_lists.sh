#!/bin/bash
cd /home/krzysztof/presence-list-generator
source venv/bin/activate
python presence_list_generator.py -m  $(($(date +%m) - 1))  -y $(date +%Y)