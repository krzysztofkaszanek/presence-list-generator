import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SPREADSHEET_ID = '1YaEO0mp3RvDd10TC4LWHrsYWR7qeoDx5TZPXAJE20X0'


def get_service():
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'client_secret_oauth.json', SCOPES)
            creds = flow.run_local_server()
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)
    return service


def apply_merge(sheet_id, start_row, end_row, start_column, end_column, action):
    service = get_service()
    requests = [{
        action: {
            'range': {
                'sheetId': sheet_id,
                'startRowIndex': start_row,
                'endRowIndex': end_row,
                'startColumnIndex': start_column,
                'endColumnIndex': end_column
            },
        }
    }]

    body = {
        'requests': requests
    }

    service.spreadsheets().batchUpdate(
        spreadsheetId=SPREADSHEET_ID,
        body=body
    ).execute()


def unmerge_all_cells(sheet_id):
    apply_merge(sheet_id, 5, 37, 0, 6, 'unmergeCells')


def merge_cells(sheet_id, start_row, end_row, start_column, end_column):
    apply_merge(sheet_id, start_row, end_row, start_column, end_column, 'mergeCells')


def apply_border(sheet_id, start_row, end_row, start_column, end_column, border_style):
    service = get_service()
    requests = [{
        'updateBorders': {
            'range': {
                'sheetId': sheet_id,
                'startRowIndex': start_row,
                'endRowIndex': end_row,
                'startColumnIndex': start_column,
                'endColumnIndex': end_column
            },
            'bottom': {
                'style': border_style,
                'width': 1,
                'color': {
                    'blue': 0,
                }
            },
            'right': {
                'style': border_style,
                'width': 1,
                'color': {
                    'blue': 0,
                }
            }
        },
    }]

    body = {
        'requests': requests
    }

    service.spreadsheets().batchUpdate(
        spreadsheetId=SPREADSHEET_ID,
        body=body
    ).execute()


def add_border(sheet_id, start_row, end_row, start_column, end_column):
    apply_border(sheet_id, start_row, end_row, start_column, end_column, 'solid')


def remove_border(sheet_id, start_row, end_row, start_column, end_column):
    apply_border(sheet_id, start_row, end_row, start_column, end_column, 'none')
