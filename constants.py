WORKSHEET_NAME='The working time and presence list(Timetastic)'

MONTHS = {
    1: 'Styczeń / January',
    2: 'Luty / February',
    3: 'Marzec / March',
    4: 'Kwiecień / April',
    5: 'Maj / May',
    6: 'Czerwiec / June',
    7: 'Lipiec / July',
    8: 'Sierpień / August',
    9: 'Wrzesień / September',
    10: 'Październik / October',
    11: 'Listopad / November',
    12: 'Grudzień / December'
}

LEAVE_TYPES = {
    'Vacation': ' Urlop / Vacations ',
    'Sick Leave': 'Chorobowe / Sick leave',
    'Maternity': 'Urlop macierzyński / Maternity leave',
    'Paternity': 'Urlop tacierzyński / Paternity leave',
    'Conference': 'Delegacja / Business trip',
    'Not working': 'Urlop / Vacations',
    'Bank holiday': 'Dzień ustawowo wolny od pracy zgodnie z art. 130 §2 KP',
    'Odbi&#243;r dnia wolnego zgodnie z art. 130 &#167; 2 KP': 'Odbiór dnia wolnego zgodnie z art. 130 § 2 KP'
}

NOT_FULL_DAY_STATUSES = ['Remote', 'In the office']

EMAIL_DOMAIN = '@kiwee.eu'

EXCLUDED_USERS = ['anna.sawicka', 'tomek', 'kalina.sokolowska', 'piotr.kaczkowski']

DAYS_ROWS_OFFSET = 5
