import datetime
import math
import time
import argparse
from calendar import monthrange
import gspread
import requests
import holidays
from gspread_formatting import CellFormat, Color, format_cell_range
from oauth2client.service_account import ServiceAccountCredentials
from google.oauth2.service_account import Credentials
from absence import Absence
from google_api_styling import remove_border, add_border, merge_cells, unmerge_all_cells
from auth_header import HEADERS
from constants import MONTHS, LEAVE_TYPES, EMAIL_DOMAIN, NOT_FULL_DAY_STATUSES, EXCLUDED_USERS,\
    WORKSHEET_NAME, DAYS_ROWS_OFFSET

SCOPE = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
CREDS = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', SCOPE)
CLIENT = gspread.authorize(CREDS)
SHEET = CLIENT.open(WORKSHEET_NAME)


def get_users_ids():
    users = requests.get('http://app.timetastic.co.uk/api/users', headers=HEADERS).json()
    users_dict = {}
    for user in users:
        users_dict[user['email']] = user['id']
    return users_dict


def get_absences(month, year, username):
    users = get_users_ids()
    user_id = users[username + EMAIL_DOMAIN]
    days_in_month = str(monthrange(year, month)[1] + 1)
    year = str(year)
    month = str(month)

    first_request = requests.get('http://app.timetastic.co.uk/api/holidays?Start=' +
                                 year + '-' + month + '-01&End=' +
                                 year + '-' + month + '-' + days_in_month,
                                 headers=HEADERS).json()

    total_records = first_request['totalRecords']
    all_absences = [first_request['holidays']]

    if total_records > 100:
        requests_left = math.ceil((total_records - 100) / 100)
        for page in range(2, requests_left + 2):
            all_absences.append(
                requests.get('http://app.timetastic.co.uk/api/holidays?Start=' +
                             year + '-' + month + '-01&End=' +
                             year + '-' + month + '-' + days_in_month + '&pageNumber=' + str(page),
                             headers=HEADERS).json()['holidays']
            )

    absences = [item for sublist in all_absences for item in sublist]
    absences_list = []
    for absence in absences:
        if absence['userId'] == user_id:
            absences_list.append(
                Absence(
                    absence['leaveType'],
                    absence['startDate'],
                    absence['endDate'],
                    absence['bookingUnit']))
    return absences_list


def is_bank_holiday(bank_holidays, day):
    for holiday in bank_holidays:
        if holiday[0].day == day:
            return True
    return False


def fill_presence_list(month, year, username):
    worksheet = SHEET.worksheet(username)
    absences = get_absences(month, year, username)

    days_in_month = monthrange(year, month)[1]
    bank_holidays = get_bank_holidays(month, year)
    cells_to_update = worksheet.range('B6:D' + str(days_in_month + DAYS_ROWS_OFFSET))

    for i in range(1, days_in_month + 1):
        day = datetime.datetime(year, month, i)
        day_status = None
        for absence in absences:
            if absence.start_date <= day <= absence.end_date and absence.absence_type in LEAVE_TYPES.keys() \
                    or (absence.start_date - day).days == 0 and not absence.whole_day:
                day_status = absence

        if not (day.weekday() == 5 or day.weekday() == 6):
            if is_bank_holiday(bank_holidays, day.day):
                merge_cells(get_user_sheet_id(username), i + (DAYS_ROWS_OFFSET-1), i + DAYS_ROWS_OFFSET, 1, 5)
                cells_to_update[((i - 1) * 3)].value = LEAVE_TYPES['Bank holiday']
            elif day_status is not None:
                if not day_status.whole_day and day_status.absence_type in NOT_FULL_DAY_STATUSES:
                    cells_to_update[((i - 1) * 3)].value = get_hour_format(day_status.start_date)
                    cells_to_update[((i - 1) * 3) + 1].value = get_hour_format(day_status.end_date)
                    cells_to_update[((i - 1) * 3) + 2].value = day_status.get_hours()
                else:
                    cells_to_update[((i - 1) * 3) + 1].value = LEAVE_TYPES[day_status.absence_type]
            else:
                cells_to_update[((i - 1) * 3)].value = '9:00'
                cells_to_update[((i - 1) * 3) + 1].value = '17:00'
                cells_to_update[((i - 1) * 3) + 2].value = '8'
    worksheet.update_cells(cells_to_update)


def get_hour_format(date):
    return date.strftime('%H:%M')


def clear_presence_list(email):
    worksheet = SHEET.worksheet(email)

    cell_list = worksheet.range('A6:D36')
    for cell in cell_list:
        cell.value = ''
    worksheet.update_cells(cell_list)


def clear_all_lists():
    users = get_users_ids()
    for email in users:
        user = email[:-len(EMAIL_DOMAIN)]
        if user not in EXCLUDED_USERS:
            clear_presence_list(user)


def create_template(month, year, username):
    worksheet = SHEET.worksheet(username)
    days_in_month = monthrange(year, month)[1]
    user_sheet_id = get_user_sheet_id(username)

    white_background = CellFormat(
        backgroundColor=Color(1, 1, 1),
    )

    grey_background = CellFormat(
        backgroundColor=Color(243 / 255, 243 / 255, 243 / 255),
    )

    format_cell_range(worksheet, 'A6:E37', white_background)
    unmerge_all_cells(user_sheet_id)
    set_center_align(username)

    for i in range(33, 37):
        remove_border(user_sheet_id, i - 1, i, 0, 5)

    add_border(user_sheet_id, days_in_month, days_in_month + DAYS_ROWS_OFFSET, 0, 5)

    days_numbers_cells = worksheet.range('A6:A' + str(days_in_month + DAYS_ROWS_OFFSET))
    for (i, cell) in enumerate(days_numbers_cells):
        cell.value = i + 1
    worksheet.update_cells(days_numbers_cells)

    for i in range(1, days_in_month + 1):
        day = datetime.datetime(year, month, i)
        worksheet.update_cell(i + DAYS_ROWS_OFFSET, 1, day.day)
        if day.weekday() in [5, 6]:  # weekend
            format_cell_range(worksheet, 'A' + str(i + DAYS_ROWS_OFFSET) + ':E' +
                              str(i + DAYS_ROWS_OFFSET), grey_background)

    worksheet.update_cell(1, 2, year)
    worksheet.update_cell(2, 2, MONTHS[month])


def create_templates_for_all_users(month, year):
    users = get_users_ids()
    for email in users:
        user = email[:-len(EMAIL_DOMAIN)]
        if user not in EXCLUDED_USERS:
            create_template(month, year, user)


def fill_all_lists(month, year):
    users = get_users_ids()
    for email in users:
        print(email)
        user = email[:-len(EMAIL_DOMAIN)]
        if user not in EXCLUDED_USERS:
            clear_presence_list(user)
            create_template(month, year, user)
            fill_presence_list(month, year, user)
            # To avoid hitting the requests limit (100 requests per 100 seconds)
            time.sleep(100)


def set_center_align(user):
    worksheet = SHEET.worksheet(user)
    align_center_format = CellFormat(
        horizontalAlignment='CENTER'
    )
    format_cell_range(worksheet, 'A3:E38', align_center_format)


def get_user_sheet_id(username):
    worksheets = SHEET.worksheets()
    return next((ws for ws in worksheets if ws.title == username), None).id


def get_bank_holidays(month, year):
    bank_holidays = []
    for holiday in holidays.Poland(years=year).items():
        date = holiday[0]
        if date.year == year and date.month == month:
            bank_holidays.append(holiday)
    return bank_holidays



parser = argparse.ArgumentParser(description='Generates presence list from Timetastic')
parser.add_argument('-m', '--month', type=int, help='Month number', required=True)
parser.add_argument('-y', '--year', type=int, help='Year', required=True)
args = parser.parse_args()

month = args.month
year = args.year

fill_all_lists(month, year)