from datetime import datetime


class Absence:
    def __init__(self, absence_type, start_date, end_date, booking_unit):
        self.absence_type = absence_type
        self.start_date = datetime.fromisoformat(start_date)
        self.end_date = datetime.fromisoformat(end_date)
        self.whole_day = booking_unit == 'Days'

    def __str__(self):
        return self.absence_type + ' ' + \
               str(self.start_date) + ' - ' + \
               str(self.end_date) + ' ' + \
               str(self.whole_day)

    def get_hours(self):
        time = str(self.end_date.replace(microsecond=0, second=0) -
                   self.start_date.replace(microsecond=0, second=0))[:-3]
        (h, m) = time.split(':')
        return (int(h) * 60 + int(m)) / 60
